import subprocess
process = subprocess.Popen([
    "./pddl_planner.py",
    "../benchmarks/blocks/domain.pddl",
    "../benchmarks/blocks/task03.pddl",
    "-H", "hadm", "-s", "astar"], stdout=subprocess.PIPE)
out = [bobject.decode('ascii') for bobject in
       process.communicate()[0].splitlines()]


def extract(keyword):
    return [s for s in out if keyword in s][0].split()[-1]
time = extract('time')
node_expansion = extract('Node')
length = extract('length')
print("run time: "+time +
      "\nnode expansion: "+node_expansion +
      "\nplan length: "+length)
#print(out.splitlines())
#splited = out.split(' ')
#print(splited)
# time_index = splited.index("time:") + 1
#print(splited[2])