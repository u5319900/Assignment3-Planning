
"""
COMP3620-6320 Artificial Intelligence 2017 - Planning Assignment Q1
Classes for representing a STRIPS planning task and capturing its semantics

Enter your details below:

Name:Dong Luo
Student Code:u5319900
email:u5319900@anu.edu.au


Implements the Greedy Best First Search (GBFS) search algorithm for planning.

Method to be implemented is gbfs.

We provide imports for some basic data-structure that can be useful to tackle 
the problem. In particular have a look at heapq that
is an efficient implementation of a priority queue using heap
"""

import heapq
import logging

from search import searchspace
from planning_task import Task
from heuristics import BlindHeuristic


def gbfs(task, heuristic=BlindHeuristic):
    """
    Searches for a plan in the given task using Greedy Best First Search search.

    @param task The task to be solved
    @param heuristic  A heuristic callable which computes the estimated steps
                      from a search node to reach the goal.
    """
    frontier = PriorityQueue()

    initial_state = task.initial_state
    root_node = searchspace.make_root_node(initial_state)

    frontier.insert(heuristic(root_node), root_node)
    explored = set()
    goal = None
    while not frontier.isEmpty():

        current_node = frontier.extractMin()
        # goal test
        if task.goal_reached(current_node.state):
            goal = current_node
            break
        explored.add(current_node)

        for successor in task.get_successor_states(current_node.state):
            new_node = searchspace.make_child_node(
                current_node, successor[0], successor[1])
            new_heuristics = heuristic(new_node)
            visited = False

            visited_node = frontier.find_same_state(new_node)

            if visited_node:
                visited = True
                if visited_node.g > new_node.g:
                    visited_node.parent = new_node.parent
                    visited_node.action = new_node.action
                    visited_node.g = new_node.g
                    frontier.changeKey(
                        new_heuristics,
                        visited_node)

            for node in list(explored):
                if node.state == new_node.state:
                    visited = True
                    if node.g > new_node.g:
                        explored.remove(node)
                        frontier.insert(
                            new_heuristics, new_node)

            if not visited:
                frontier.insert(new_heuristics, new_node)
    if goal:
        return goal.extract_solution()
    else:
        return []


class PriorityQueue:
    '''
    A Priority Queue for SearchNode
    '''

    def __init__(self):
        self.heap = []
        self.count = 0

    def insert(self, key, element):
        heapq.heappush(self.heap, (key, self.count, element))
        self.count += 1

    def extractMin(self):
        return heapq.heappop(self.heap)[2]

    def isEmpty(self):
        return not self.heap

    def find_same_state(self, node):
        """
        Return the node which has the same state
        Return None if there is no such node
        """
        for elem in self.heap:
            if elem[2].state == node.state:
                return elem[2]
        return None

    def changeKey(self, key, element):
        for index, content in enumerate(self.heap):
            if content[1] == element:
                self.heap[index] = (key, self.count, element)
                self.count += 1
                heapq.heapify(self.heap)
                break
